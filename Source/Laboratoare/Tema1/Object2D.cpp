#include "Object2D.h"

#include <Core/Engine.h>

Mesh* Object2D::CreateSquare(std::string name, glm::vec3 leftBottomCorner, float length, glm::vec3 color, bool fill)
{
	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner, color),
		VertexFormat(corner + glm::vec3(length, 0, 0), color),
		VertexFormat(corner + glm::vec3(length, length, 0), color),
		VertexFormat(corner + glm::vec3(0, 1, 0), color)
	};

	Mesh* square = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2, 3};

	if (!fill) {
		square->SetDrawMode(GL_LINE_LOOP);
	}
	else {
		indices.push_back(0);
		indices.push_back(2);
	}

	square->InitFromData(vertices, indices);
	return square;
}

Mesh* Object2D::CreateRectangle(std::string name, glm::vec3 leftBottomCorner, float l, float length, glm::vec3 color, bool fill)
{
	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner, color),
		VertexFormat(corner + glm::vec3(length, 0, 0), color),
		VertexFormat(corner + glm::vec3(length, l, 0), color),
		VertexFormat(corner + glm::vec3(0, l, 0), color)
	};

	Mesh* rectangle = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2, 3};
	
	if (!fill) {
		rectangle->SetDrawMode(GL_LINE_LOOP);
	}
	else {
		indices.push_back(0);
		indices.push_back(2);
	}

	rectangle->InitFromData(vertices, indices);
	return rectangle;
}
Mesh* Object2D::CreateTriangle(std::string name, glm::vec3 leftBottomCorner, float length, glm::vec3 color, bool fill)
{
	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner + glm::vec3(length, 0, 0), color),
		VertexFormat(corner + glm::vec3(length, length, 0), color),
		VertexFormat(corner + glm::vec3(0, length, 0), color)
	};

	Mesh* triangle = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2};

	if (!fill) {
		triangle->SetDrawMode(GL_LINE_LOOP);
	}
	else {

		indices.push_back(0);
		indices.push_back(2);
	}

	triangle->InitFromData(vertices, indices);
	return triangle;

}
Mesh* Object2D::CreateCircle(std::string name, glm::vec3 Centre, float r, glm::vec3 color, bool fill)
{
	glm::vec3 corner = Centre;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner + glm::vec3(-r, 0, 0), color),
		VertexFormat(corner + glm::vec3(-r * 0.9, -r * 0.35, 0), color),
		VertexFormat(corner + glm::vec3(-r * 0.7, -r * 0.7, 0), color),
		VertexFormat(corner + glm::vec3(-r * 0.4, -r * 0.9, 0), color),

		VertexFormat(corner + glm::vec3(0, -r, 0), color),
		VertexFormat(corner + glm::vec3(r * 0.4, -r * 0.9, 0), color),
		VertexFormat(corner + glm::vec3(r * 0.7, -r * 0.7, 0), color),
		VertexFormat(corner + glm::vec3(r * 0.9, -r * 0.35, 0), color),

		VertexFormat(corner + glm::vec3(r, 0, 0), color),
		VertexFormat(corner + glm::vec3(r * 0.9, r * 0.4, 0), color),
		VertexFormat(corner + glm::vec3(r * 0.7, r * 0.7, 0), color),
		VertexFormat(corner + glm::vec3(r * 0.4, r * 0.9, 0), color),

		VertexFormat(corner + glm::vec3(0, r, 0), color),
		VertexFormat(corner + glm::vec3(-r * 0.4, r * 0.9, 0), color),
		VertexFormat(corner + glm::vec3(-r * 0.7, r * 0.7, 0), color),
		VertexFormat(corner + glm::vec3(-r * 0.9, r * 0.4, 0), color),
	};

	Mesh* circle = new Mesh(name);
	std::vector<unsigned short> indices =
	{
		1, 2, 0,
		2, 3, 0,
		3, 4, 0,
		4, 5, 0,
		5, 6, 0,
		6, 7, 0,
		7, 8, 0,
		8, 9, 0,
		9, 10, 0,
		10, 11, 0,
		11, 12, 0,
		12, 13, 0,
		13, 14, 0,
		14, 15, 0,
		15, 16, 0,
		16, 1, 0
	};

	if (!fill) {
		circle->SetDrawMode(GL_LINE_LOOP);
	}
	else {
		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);
		indices.push_back(3);
		indices.push_back(4);
		indices.push_back(5);
		indices.push_back(6);
		indices.push_back(7);
		indices.push_back(8);
		indices.push_back(9);
		indices.push_back(10);
		indices.push_back(11);
		indices.push_back(12);
		indices.push_back(13);
		indices.push_back(14);
		indices.push_back(15);
		indices.push_back(16);
	}

	circle->InitFromData(vertices, indices);
	return circle;
}
