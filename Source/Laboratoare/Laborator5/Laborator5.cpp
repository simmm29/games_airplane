#include "Laborator5.h"

#include <vector>
#include <string>
#include <iostream>

#include <Core/Engine.h>
//#include "Transform3D.h"

using namespace std;

Laborator5::Laborator5()
{
}

Laborator5::~Laborator5()
{
}

void Laborator5::Init()
{
	renderCameraTarget = false;
	mouse = 0;
	numarStele = 0;

	angularStepOX = 0;
	angularStepOY = 0;
	angularStepOZ = 0;
	state1 = 0;

	translateX = 0;
	translateY = 0;
	translateZ = 0;

	scaleX = 1;
	scaleY = 1;
	scaleZ = 1;
	scaleMin = 0.3;
	scaleMax = 1;
	state2 = 0;

	camera = new Laborator::Camera();
	camera->Set(glm::vec3(0, 2, 3.5f), glm::vec3(0, 1, 0), glm::vec3(0, 1, 0));

	//================================================== SEA =====================================================================================================
	{
		Mesh* mesh = new Mesh("sea");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "cilindru13.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("box");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	//================================================= AIRPLANE =================================================================================================
	/*	// plane body
	{
		Mesh* mesh = new Mesh("plane");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "plane1.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	// front of the plane
	{
		Mesh* mesh = new Mesh("front");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "front2.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	// back of the plane
	{
		Mesh* mesh = new Mesh("back1");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "back1.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}


	// rudder of the plane
	{
		Mesh* mesh = new Mesh("rudder");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "rudder1.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	// wing of the plane
	{
		Mesh* mesh = new Mesh("wing");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "wing1.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	// spinner of the plane
	{
		Mesh* mesh = new Mesh("propeller2");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "propeller1.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	// spinner2 of the plane
	/*
	{
		Mesh* mesh = new Mesh("spinner2");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "spinner2.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}*/
	//{
	//	Mesh* mesh = new Mesh("propeller1");
	//	mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "propeller4.obj");
	//	meshes[mesh->GetMeshID()] = mesh;
	//}


//================================================== CLOUDS ==================================================================================================
	{
		Mesh* mesh = new Mesh("cloud1");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "cl4.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("cloud");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "cl6.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("cloud2");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "cl9.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	//===================================================== HEARTS ===============================================================================================
	{
		Mesh* mesh = new Mesh("heart");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "h5.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	//===================================================== FUEL =================================================================================================
	{
		Mesh* mesh = new Mesh("fuel");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "fuel5.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	//===================================================== OBSTACLES =================================================================================================
	{
		Mesh* mesh = new Mesh("obstacol1");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "obs1.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}


	projectionMatrix = glm::perspective(RADIANS(60), window->props.aspectRatio, 0.01f, 200.0f);
}

void Laborator5::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(1.000, 0.714, 0.757, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Laborator5::Update(float deltaTimeSeconds)
{
	camera->TranslateRight(deltaTimeSeconds / 10);
	translateX += deltaTimeSeconds / 10;
	//================================================== SEA =====================================================================================================
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0, -4.5f, 0));
		//	translateX += deltaTimeSeconds / 10;
			//camera->TranslateRight(deltaTimeSeconds / 10);
		modelMatrix *= Transform3D::Translate(translateX, -4.5f, 0);
		RenderMesh(meshes["sea"], shaders["VertexNormal"], modelMatrix);
	}

	//================================================== AIRPLANE ================================================================================================

	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.5f, -1.0f, -1.5f));
		//translateX += deltaTimeSeconds * 2;
	//	camera->TranslateRight(deltaTimeSeconds * 2);
		modelMatrix *= Transform3D::Translate(translateX, 1.0f, -1.5f);
		RenderMesh(meshes["plane"], shaders["VertexNormal"], modelMatrix);
	}*/

	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.25f, -1.1f, -1.5f));
		////translateX += deltaTimeSeconds * 2;
		//camera->TranslateRight(deltaTimeSeconds * 2);
		modelMatrix *= Transform3D::Translate(translateX, 1.001f, -1.5f);
		RenderMesh(meshes["box"], shaders["VertexNormal"], modelMatrix);
	}

	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.9f, -1.08f, -1.5f));
		//translateX += deltaTimeSeconds * 2;
		camera->TranslateRight(deltaTimeSeconds * 2);
		modelMatrix *= Transform3D::Translate(translateX, 1.08f, -1.5f);
		RenderMesh(meshes["back1"], shaders["VertexNormal"], modelMatrix);
	}
	*/

	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-1.08f, -0.9f, -1.5f));
		//translateX += deltaTimeSeconds * 2;
		//camera->TranslateRight(deltaTimeSeconds * 2);
		modelMatrix *= Transform3D::Translate(translateX, 1.08f, -1.5f);
		RenderMesh(meshes["box"], shaders["VertexNormal"], modelMatrix);
	}

	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.2f, -0.98f, -1.5f));
		if (state1 == 0) {
			angularStepOX += deltaTimeSeconds * 0.5;
			angularStepOY += deltaTimeSeconds * 0.5;
			angularStepOZ += deltaTimeSeconds * 0.5;
		}
		translateX += deltaTimeSeconds * 2;
		camera->TranslateRight(deltaTimeSeconds * 2);
		modelMatrix *= Transform3D::Translate(translateX, 1.02f, -1.5f);
		modelMatrix *= Transform3D::RotateOX(angularStepOX);
		RenderMesh(meshes["propeller2"], shaders["VertexNormal"], modelMatrix);
	}
	/*
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-3.75f, 1.02f, -1.5f));
		if (state1 == 0) {
			angularStepOX += deltaTimeSeconds * 0.5;
			angularStepOY += deltaTimeSeconds * 0.5;
			angularStepOZ += deltaTimeSeconds * 0.5;
		}
		modelMatrix *= Transform3D::RotateOX(-angularStepOX);
		RenderMesh(meshes["propeller2"], shaders["VertexNormal"], modelMatrix);
	}

	glm::mat4 modelMatrix = glm::mat4(1);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-3.75f, 1.02f, -1.5f));
	if (state1 == 0) {
		angularStepOX += deltaTimeSeconds * 2;
		angularStepOY += deltaTimeSeconds * 2;
		angularStepOZ += deltaTimeSeconds * 2;
	}
	modelMatrix *= Transform3D::RotateOX(angularStepOX);
	RenderMesh(meshes["propeller1"], shaders["VertexNormal"], modelMatrix);*/


	//===================================================== CLOUDS ===============================================================================================
	/*		// first cloud
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(7.0f, 2.19f, 0));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			translateX += deltaTimeSeconds / 100;
			camera->TranslateRight(deltaTimeSeconds / 100);
			modelMatrix *= Transform3D::Translate(translateX, 2.19f, 0);
			modelMatrix *= Transform3D::RotateOX(angularStepOX);
			RenderMesh(meshes["cloud1"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(7.3f, 2.19f, 0));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			translateX += deltaTimeSeconds / 100;
			camera->TranslateRight(deltaTimeSeconds / 100);
			modelMatrix *= Transform3D::Translate(translateX, 2.19f, 0);
			modelMatrix *= Transform3D::RotateOY(angularStepOY);
			RenderMesh(meshes["cloud"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.93f, 1.5f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
			RenderMesh(meshes["cloud2"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.60f, 1.5f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
			RenderMesh(meshes["cloud2"], shaders["VertexNormal"], modelMatrix);
		}
/*
		// second cloud
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(1.75f, 2.45f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOY(angularStepOY);
			RenderMesh(meshes["cloud1"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(2.05f, 2.42f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOX(angularStepOX);
			RenderMesh(meshes["cloud"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(1.93f, 2.40f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
			RenderMesh(meshes["cloud2"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(1.60f, 2.40f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOX(angularStepOX);
			RenderMesh(meshes["cloud2"], shaders["VertexNormal"], modelMatrix);
		}

		//third cloud
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(3.75f, 2.25f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
			RenderMesh(meshes["cloud1"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(4.05f, 2.22f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOX(angularStepOX);
			RenderMesh(meshes["cloud"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(3.93f, 2.20f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
			RenderMesh(meshes["cloud2"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(3.60f, 2.20f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOX(angularStepOX);
			RenderMesh(meshes["cloud2"], shaders["VertexNormal"], modelMatrix);
		}

		//fourth cloud
		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(5.75f, 2.0f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOY(angularStepOY);
			RenderMesh(meshes["cloud1"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(6.05f, 2.0f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
			RenderMesh(meshes["cloud"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(5.93f, 2.0f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
			RenderMesh(meshes["cloud2"], shaders["VertexNormal"], modelMatrix);
		}

		{
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(5.60f, 2.0f, -1.0f));
			if (state1 == 0) {
				angularStepOX += deltaTimeSeconds * 0.2;
				angularStepOY += deltaTimeSeconds * 0.2;
				angularStepOZ += deltaTimeSeconds * 0.2;
			}
			modelMatrix *= Transform3D::RotateOY(angularStepOY);
			RenderMesh(meshes["cloud2"], shaders["VertexNormal"], modelMatrix);
		}*/

		// =============================================== HEARTS ====================================================================================================
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-5.6f, 1.4f, -1.5f));
		if (scaleX > scaleMax) {
			state2 = 1;
		}

		if (scaleX < scaleMin) {
			state2 = 0;
		}

		if (state2 == 0) {
			scaleX += deltaTimeSeconds * 0.5;
			scaleY += deltaTimeSeconds * 0.5;
			scaleZ += deltaTimeSeconds * 0.5;
		}
		else {
			scaleX -= deltaTimeSeconds * 0.5;
			scaleY -= deltaTimeSeconds * 0.5;
			scaleZ -= deltaTimeSeconds * 0.5;
		}
		//translateX += deltaTimeSeconds;
		//camera->TranslateRight(deltaTimeSeconds);
		modelMatrix *= Transform3D::Translate(translateX, 1.7f, -1.5f);
		modelMatrix *= Transform3D::Scale(scaleX, scaleY, scaleZ);
		RenderMesh(meshes["heart"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-5.1f, 1.4f, -1.5f));
		if (scaleX > scaleMax) {
			state2 = 1;
		}

		if (scaleX < scaleMin) {
			state2 = 0;
		}

		if (state2 == 0) {
			scaleX += deltaTimeSeconds * 0.5;
			scaleY += deltaTimeSeconds * 0.5;
			scaleZ += deltaTimeSeconds * 0.5;
		}
		else {
			scaleX -= deltaTimeSeconds * 0.5;
			scaleY -= deltaTimeSeconds * 0.5;
			scaleZ -= deltaTimeSeconds * 0.5;
		}

		//translateX += deltaTimeSeconds;
		//camera->TranslateRight(deltaTimeSeconds);
		modelMatrix *= Transform3D::Translate(translateX, 1.7f, -1.5f);
		modelMatrix *= Transform3D::Scale(scaleX, scaleY, scaleZ);
		RenderMesh(meshes["box"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-4.6f, 1.4f, -1.5f));
		if (scaleX > scaleMax) {
			state2 = 1;
		}

		if (scaleX < scaleMin) {
			state2 = 0;
		}

		if (state2 == 0) {
			scaleX += deltaTimeSeconds * 0.5;
			scaleY += deltaTimeSeconds * 0.5;
			scaleZ += deltaTimeSeconds * 0.5;
		}
		else {
			scaleX -= deltaTimeSeconds * 0.5;
			scaleY -= deltaTimeSeconds * 0.5;
			scaleZ -= deltaTimeSeconds * 0.5;
		}
	//	translateX += deltaTimeSeconds;
		//camera->TranslateRight(deltaTimeSeconds);
		modelMatrix *= Transform3D::Translate(translateX, 1.7f, -1.5f);
		modelMatrix *= Transform3D::Scale(scaleX, scaleY, scaleZ);
		RenderMesh(meshes["box"], shaders["VertexNormal"], modelMatrix);
	}

	//==================================================== FUEL ==================================================================================================
		// first chain of fuel
	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(7, 2.41f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(7.2f, 2.43f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(7.4f, 2.45f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
		numarStele++;
	}

	//second chain of fuel
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(40, 0.5f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(40.2f, 0.52f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(40.4f, 0.54f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(40.6f, 0.54f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
	}

	//third chain of fuel
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(60, 1.27f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(60.2f, 1.29f, 0));
		if (state1 == 0) {
			angularStepOZ += deltaTimeSeconds;
		}
		modelMatrix *= Transform3D::RotateOZ(angularStepOZ);
		translateX += deltaTimeSeconds / 100;
		camera->TranslateRight(deltaTimeSeconds / 100);
		RenderMesh(meshes["fuel"], shaders["VertexNormal"], modelMatrix);
	}

	//================================================================= OBSTACLES ================================================================================
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(50.0f, 1.84f, 0));
		RenderMesh(meshes["obstacol1"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(57.0f, 2.0f, 0));
		RenderMesh(meshes["obstacol1"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(20.0f, 1.15f, 0));
		RenderMesh(meshes["obstacol1"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(35.0f, 1.3f, 0));
		RenderMesh(meshes["obstacol1"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(67.0f, 0.3f, 0));
		RenderMesh(meshes["obstacol1"], shaders["VertexNormal"], modelMatrix);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(80.0f, 1.5f, 0));
		RenderMesh(meshes["obstacol1"], shaders["VertexNormal"], modelMatrix);
	}
	*/
}

void Laborator5::FrameEnd()
{
	//DrawCoordinatSystem(camera->GetViewMatrix(), projectionMatrix);
}
void Laborator5::RenderSimpleMesh(Mesh* mesh, Shader* shader, const glm::mat4& modelMatrix, const glm::vec3& color)
{
	if (!mesh || !shader || !shader->GetProgramID())
		return;

	// render an object using the specified shader and the specified position
	glUseProgram(shader->program);

	// Set shader uniforms for light & material properties
	// TODO: Set light position uniform

	// TODO: Set eye position (camera position) uniform
	//glm::vec3 eyePosition = GetSceneCamera()->transform->GetWorldPosition();

	// TODO: Set material property uniforms (shininess, kd, ks, object color) 

	// Bind model matrix
	GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
	glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	// Bind view matrix
	//glm::mat4 viewMatrix = GetSceneCamera()->GetViewMatrix();
	//int loc_view_matrix = glGetUniformLocation(shader->program, "View");
	//glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	// Bind projection matrix
	glm::mat4 projectionMatrix = GetSceneCamera()->GetProjectionMatrix();
	int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
	glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	// Draw the object
	glBindVertexArray(mesh->GetBuffers()->VAO);
	glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}
void Laborator5::RenderMesh(Mesh* mesh, Shader* shader, const glm::mat4& modelMatrix)
{
	if (!mesh || !shader || !shader->program)
		return;

	// render an object using the specified shader and the specified position
	shader->Use();
	glUniformMatrix4fv(shader->loc_view_matrix, 1, GL_FALSE, glm::value_ptr(camera->GetViewMatrix()));
	glUniformMatrix4fv(shader->loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
	glUniformMatrix4fv(shader->loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	mesh->Render();
}

//float FoV = 60, width = 10, height = 10;

void Laborator5::OnInputUpdate(float deltaTime, int mods)
{
	// move the camera only if MOUSE_RIGHT button is pressed
	if (window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		float cameraSpeed = 2.0f;
	}
}

void Laborator5::OnKeyPress(int key, int mods)
{
	// add key press event
	if (key == GLFW_KEY_T)
	{
		renderCameraTarget = !renderCameraTarget;
	}
}

void Laborator5::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator5::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
	if (window->MouseHold(GLFW_MOUSE_BUTTON_LEFT)) {
		mouse = 1;
		dty = float(deltaY);
	}

	if (window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		float sensivityOX = 0.001f;
		float sensivityOY = 0.001f;

		if (window->GetSpecialKeyState() == 0) {
			renderCameraTarget = false;
			// TODO : rotate the camera in First-person mode around OX and OY using deltaX and deltaY
			// use the sensitivity variables for setting up the rotation speed
			camera->RotateFirstPerson_OX(-deltaY * sensivityOX);
			camera->RotateFirstPerson_OY(-deltaX * sensivityOY);
		}

		if (window->GetSpecialKeyState() && GLFW_MOD_CONTROL) {
			renderCameraTarget = true;
			// TODO : rotate the camera in Third-person mode around OX and OY using deltaX and deltaY
			// use the sensitivity variables for setting up the rotation speed
			camera->RotateThirdPerson_OX(-deltaY * sensivityOX);
			camera->RotateThirdPerson_OY(-deltaX * sensivityOY);
		}

	}
}

void Laborator5::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator5::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator5::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator5::OnWindowResize(int width, int height)
{
}

