#include "Laborator4.h"

#include <vector>
#include <string>
#include <iostream>

#include <Core/Engine.h>
#include "Transform3D.h"

using namespace std;

Laborator4::Laborator4()
{
}

Laborator4::~Laborator4()
{
}

void Laborator4::Init()
{
	polygonMode = GL_FILL;

	Mesh* mesh = new Mesh("box");
	mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
	meshes[mesh->GetMeshID()] = mesh;

	// initialize tx, ty and tz (the translation steps)
	translateX = 0;
	translateY = 0;
	translateZ = 0;

	// initialize sx, sy and sz (the scale factors)
	scaleX = 1;
	scaleY = 1;
	scaleZ = 1;
	ok = 0;
	// initialize angularSteps
	angularStepOX = 0;
	angularStepOY = 0;
	angularStepOZ = 0;
	float moveOy = 0;
	float rotateStep = 0;
}

void Laborator4::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Laborator4::Update(float deltaTimeSeconds)
{
		modelMatrix = glm::mat4(1);

		modelMatrix *= Transform3D::Translate(translateX, 0.0f, 0.0f);
		modelMatrix *= Transform3D::RotateOZ(angularStepOX);
		modelMatrix *= Transform3D::Translate(1.5, 0.0f, 0.0f);
		RenderMesh(meshes["box"], shaders["VertexNormal"], modelMatrix);

	angularStepOX += deltaTimeSeconds;
	if (angularStepOX > 3.14) {
		angularStepOX = 0;
	}
}

void Laborator4::FrameEnd()
{
	DrawCoordinatSystem();
}

void Laborator4::OnInputUpdate(float deltaTime, int mods)
{
	//translatie Oy primul cub

	if (window->KeyHold(GLFW_KEY_W))
		translateY += deltaTime;
	if (window->KeyHold(GLFW_KEY_S))
		translateY -= deltaTime;


	//translatie al doilea cub
	if (window->KeyHold(GLFW_KEY_T)) {
		scaleX += deltaTime;
		scaleY += deltaTime;
		scaleZ += deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_Y)) {
		scaleX -= deltaTime;
		scaleY -= deltaTime;
		scaleZ -= deltaTime;
	}

	//angular step - 6 taste necesare
	if (window->KeyHold(GLFW_KEY_Z))
		angularStepOX -= deltaTime;
	if (window->KeyHold(GLFW_KEY_X)) //spre dreapta
		angularStepOX += deltaTime;
	if (window->KeyHold(GLFW_KEY_C))
		angularStepOY -= deltaTime;
	if (window->KeyHold(GLFW_KEY_V))
		angularStepOY += deltaTime;
	if (window->KeyHold(GLFW_KEY_B))
		angularStepOZ -= deltaTime;
	if (window->KeyHold(GLFW_KEY_N))
		angularStepOZ += deltaTime;
}

void Laborator4::OnKeyPress(int key, int mods)
{

	// add key press event
	if (key == GLFW_KEY_SPACE)
	{
		switch (polygonMode)
		{
		case GL_POINT:
			polygonMode = GL_FILL;
			break;
		case GL_LINE:
			polygonMode = GL_POINT;
			break;
		default:
			polygonMode = GL_LINE;
			break;
		}
	}
}

void Laborator4::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator4::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
}

void Laborator4::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator4::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator4::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator4::OnWindowResize(int width, int height)
{
}
